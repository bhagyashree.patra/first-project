#!/usr/bin/env python3

import re

# Pattern of 5 char string which ends with 'cky'
def check_end(string):
    pattern = '..cky$'
    if re.match(pattern, string):
        return 'Valid'
    else:
        return 'Invalid'

def check_end_2(string):
    pattern = re.compile('..cky$')
    if pattern.match(string):
        return 'Valid'
    else:
        return 'Invalid'

# Any string starting with 'L'
def check_begin(string):
    pattern = '^Lu'
    if re.match(pattern, string):
        return 'Valid'
    else:
        return 'Invalid'

# Any string ending with 'y'
def only_end(string):
    pattern = re.compile(r'y$')
    if pattern.search(string):
        return 'Valid'
    else:
        return 'Invalid'

# anything inside a range to match in a string
def check_any(string):
    pattern = re.compile(r'[a-c]') # [abc]
    return len(pattern.findall(string))

# anything inside a range to match the number
def check_any_number(number):
    pattern = re.compile(r'[0-39]') # [1239]
    return len(pattern.findall(str(number)))

# anything othar than the range
def check_inv_any(string):
    pattern = re.compile(r'[^a-c]') # [^abc]
    return len(pattern.findall(string))

def check_non_digit(string):
    pattern = re.compile(r'[^0-9]')
    return len(pattern.findall(string))

# A period matches any single character (except newline '\n')
# one dot refers to one character
# two dots refers to two characters
# two dots pattern, four char string; match: 2
# two dots pattern, 5 char string; match: 2
# two dots pattern, 6 char string; match: 3
def periodic_check(string):
    pattern = re.compile(r'..')
    return len(pattern.findall(string))

def star(string):
    pattern = re.compile(r'ma*n')
    return len(pattern.findall(string))

def plus(string):
    pattern = re.compile(r'ma+n')
    return len(pattern.findall(string))

def question(string):
    pattern = re.compile(r'ma?n')
    return len(pattern.findall(string))

def least_most(string):
    pattern = re.compile(r'a{2,3}')
    return len(pattern.findall(string))

def least_most_digit(string):
    pattern = re.compile(r'[0-9]{2,4}')
    return len(pattern.findall(string))

def alt(string):
    pattern = re.compile(r'a|b')
    return len(pattern.findall(string))

def group(string):
    pattern = re.compile(r'(a|b|c)mn')
    return len(pattern.findall(string))