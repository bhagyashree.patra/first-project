from regex import *

def test_beginning():
    assert beginning('Lucky') == 'Valid'
    assert beginning('Ludo') == 'Valid'
    assert beginning('lucky') == 'Valid'
    assert beginning('Chhunu') == 'Invalid'
    assert beginning('run') == 'Valid'

def test_beginning_2():
    assert beginning_2('Lucky') == 'Valid'
    assert beginning_2('Ludo') == 'Valid'
    assert beginning_2('lucky') == 'Invalid'
    assert beginning_2('Chhunu') == 'Invalid'
    assert beginning_2('run') == 'Invalid'

def test_beginning_3():
    assert beginning_3('the sun') == 'Valid'
    assert beginning_3('in the sun') == 'Invalid'
    assert beginning_3('theory') == 'Valid'

def test_beginning_4():
    assert beginning_4('the sun') == 'Valid'
    assert beginning_4('in the sun') == 'Invalid'
    assert beginning_3('theory') == 'Valid'

def test_ending():
    assert ending('Lucky') == 'Valid'
    assert ending('Ludo') == 'Invalid'
    assert ending('lucky') == 'Valid'
    assert ending('Tiki') == 'Invalid'
    assert ending('Vicky') == 'Valid'

def test_ending_2():
    assert ending_2('Lucky') == 'Valid'
    assert ending_2('Ludo') == 'Invalid'
    assert ending_2('lucky') == 'Valid'
    assert ending_2('Tiki') == 'Invalid'
    assert ending_2('Vicky') == 'Valid'

def test_beginning_or_ending():
    assert beginning_or_ending('the sun') == 'Valid'
    assert beginning_or_ending('in the sun') == 'Invalid'
    assert beginning_or_ending('theory') == 'Valid'
    assert beginning_or_ending('the moon follow the') == 'Valid'
    assert beginning_or_ending('moon follow the') == 'Valid'

def test_beginning_or_ending_word():
    assert beginning_or_ending_word('the sun') == 'Valid'
    assert beginning_or_ending_word('in the sun') == 'Valid'
    assert beginning_or_ending_word('theory') == 'Valid'
    assert beginning_or_ending_word('the moon follow the') == 'Valid'
    assert beginning_or_ending_word('moon follow the') == 'Valid'
    assert beginning_or_ending_word('catherine') == 'Invalid'

def test_not_beginning_or_ending_word():
    assert not_beginning_or_ending_word('the sun') == 'Invalid'
    assert not_beginning_or_ending_word('in the sun') == 'Invalid'
    assert not_beginning_or_ending_word('theory') == 'Invalid' # for 'the\\B' Valid
    assert not_beginning_or_ending_word('the moon follow the') == 'Invalid'
    assert not_beginning_or_ending_word('moon follow the') == 'Invalid'
    assert not_beginning_or_ending_word('catherine') == 'Valid'
    assert not_beginning_or_ending_word('pathe') == 'Invalid' # for '\\Bthe' Valid

def test_anywhere():
    assert anywhere('Lucky') == 'Valid'
    assert anywhere('Ludo') == 'Invalid'
    assert anywhere('lucky') == 'Valid'
    assert anywhere('Tiki') == 'Invalid'
    assert anywhere('Vicky') == 'Valid'

def test_anything_from_set():
    assert anything_from_set('Lucky') == ['c','k']
    assert anything_from_set('Ludo') == 'Invalid'
    assert anything_from_set('lucky') == ['c','k']
    assert anything_from_set('Tiki') == ['k']
    assert anything_from_set('Vicky') == ['c','k']

def test_nothing_from_set():
    assert nothing_from_set('Lucky') == ['L','u','y']
    assert nothing_from_set('Ludo') == ['L','u','d','o']
    assert nothing_from_set('lucky') == ['l','u','y']
    assert nothing_from_set('Tiki') == ['T','i','i']
    assert nothing_from_set('Vicky') == ['V','i','y']

def test_nothing_from_set_range():
    assert nothing_from_set_range('Lucky') == ['L','u','y'] # 'L' is not in the range, but 'l' is
    assert nothing_from_set_range('Ludo') == ['L','u'] # 'o' is not included in the range
    assert nothing_from_set_range('lucky') == ['u','y']
    assert nothing_from_set_range('Tiki') == ['T']
    assert nothing_from_set_range('Vicky') == ['V','y']

def test_alternation():
    assert alternation('Lucky') == (3, ['u','c','k'])
    assert alternation('Ludo') == (1, ['u'])
    assert alternation('lucky') == (3, ['u','c','k'])
    assert alternation('Tiki') == (1, ['k'])
    assert alternation('Vicky') == (2, ['c','k'])

def test_group():
    assert group('Lucky') == ['c']
    assert group('Ludo') == None
    assert group('lucky') == ['c']
    assert group('Tiki') == None
    assert group('Vicky') == ['c']
    assert group('Lucky Vicky Ticky Hacky') == ['c', 'c', 'c', 'c']

def test_min_max():
    assert min_max('ll') == (1,['ll'])
    assert min_max('lll') == (1,['lll'])
    assert min_max('llll') == (1,['lll'])
    assert min_max('lllll') == (2,['lll', 'll'])
    assert min_max('ll ll l l') == (2,['ll','ll'])

def test_min_max_range():
    assert min_max_range('ab123csde') == (1,['123'])
    assert min_max_range('12 and 345673') == (3,['12','3456','73'])
    assert min_max_range('1 and 2') == 'No Match'

def test_min_max_out_of_range():
    assert min_max_out_of_range('ab123csde') == (2,['ab','csd'])
    assert min_max_out_of_range('12 and 345673') == (2,[' an', 'd '])
    assert min_max_out_of_range('1a2') == 'No Match'

def test_zero_or_more():
    assert zero_or_more('mn') == (1,['mn'])
    assert zero_or_more('mnmn') == (2,['mn','mn'])
    assert zero_or_more('n') == (0,[])
    assert zero_or_more('a') == (0,[])
    assert zero_or_more('m') == (0,[])
    assert zero_or_more('an') == (0,[])
    assert zero_or_more('man') == (1, ['man'])
    assert zero_or_more('maaan') == (1, ['maaan'])
    assert zero_or_more('woman') == (1, ['man'])
    assert zero_or_more('main') == (0,[])
    assert zero_or_more('amamaan') == (1, ['maan'])

def test_one_or_more():
    assert one_or_more('mn') == 0
    assert one_or_more('man') == 1
    assert one_or_more('maaan') == 1
    assert one_or_more('main') == 0
    assert one_or_more('an') == 0

def test_zero_or_one():
    assert zero_or_one('mn') == 1
    assert zero_or_one('maan') == 0
    assert zero_or_one('main') == 0
    assert zero_or_one('an') == 0
    assert zero_or_one('man') == 1

def test_digits():
    assert digits('mn 89hj5') == ['8', '9', '5']
    assert digits('nxjs') == []

def test_spaces():
    assert spaces('mn 89hj5') == [' ']
    assert spaces('mn   89hj5') == [' ', ' ', ' ']
    assert spaces('mn\n89hj5') == ['\n']
    assert spaces('mn\t89hj5') == ['\t']
    assert spaces('mn\r89hj5') == ['\r']
    assert spaces('mn\v89hj5') == ['\v']
    assert spaces('mn\f89hj5') == ['\f']

def test_alpha_numeric():
    assert alpha_numeric('m$n #89hj5') == ['m', 'n', '8', '9', 'h', 'j', '5']
    assert alpha_numeric('nxjs') == ['n','x','j','s']

def test_special_chars():
    assert special_chars('m$n #89hj5') == ['$',' ','#']
    assert special_chars('nxjs') == []