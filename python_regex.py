#!/usr/bin/env python3

import re
from tracemalloc import start

def find_string(string):
    pattern = 'the'
    found = re.findall(pattern, string)
    return len(found)

def find_digit(string):
    pattern = '\\d{2,3}'
    found = re.findall(pattern, string)
    return found

def find_digit_with_plus(string):
    pattern = '\\d+'
    found = re.findall(pattern, string)
    return found

def splitting_by_space(string):
    pattern = ' '
    found = re.split(pattern, string)
    return found

def splitting_by_digit(string):
    pattern = '\\d+'
    found = re.split(pattern, string)
    return found

def splitting_using_max_split_arg(string, max_split):
    pattern = '\\d+'
    found = re.split(pattern, string, maxsplit=max_split)
    return found

def replace_string(string):
    pattern = 'day'
    replace = 'go'
    found = re.sub(pattern, replace, string)
    return found

def replace_with_count(string, count):
    pattern = 'day'
    replace = 'go'
    found = re.sub(pattern, replace, string, count=count)
    return found

def replace_with_count_tuple(string, count):
    pattern = 'day'
    replace = 'go'
    found = re.subn(pattern, replace, string, count=count)
    return found

def search_string_start_index(string):
    pattern = 'day'
    found = re.search(pattern, string)
    return found.start()

def search_string_end_index(string):
    pattern = 'day'
    found = re.search(pattern, string)
    return found.end()

def search_string_span(string):
    pattern = 'day'
    found = re.search(pattern, string)
    return found.span()

# two letters followed by a space and 'd'
def match_string(string):
    pattern = '(\\w{2}) (d)'
    match = re.search(pattern, string)
    group = match.group()
    group1 = match.group(1)
    group2 = match.group(2)
    groups = match.groups()
    start = match.start()
    end = match.end()
    span = match.span()
    regex = match.re
    

    return group, group1, group2, groups, start, end, span, regex

def get_string(string):
    pattern = '(\\w{2}) (d)'
    match = re.search(pattern, string)
    return match.string

def replace_string_with_group(string):
    pattern = '(\\w{2}) (d)'
    match = re.search(pattern, string)
    group = match.group()
    string = string.replace(group, '<b>' + group + '</b>')
    return string
