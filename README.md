# MetaCharacters
    Metacharacters are characters that are interpreted in a special way by a RegEx engine. Here's a list of metacharacters:

    [] . ^ $ * + ? {} () \ |