from reg import *

def test_check_end():
    assert check_end('Lucky') == 'Valid'
    assert check_end('Vicky') == 'Valid'
    assert check_end('LuckyVicky') == 'Invalid'
    assert check_end('Luck') == 'Invalid'
    assert check_end('Lucky123') == 'Invalid'
    assert check_end('ucky') == 'Invalid'

def test_check_end_2():
    assert check_end_2('Lucky') == 'Valid'
    assert check_end_2('Vicky') == 'Valid'
    assert check_end_2('LuckyVicky') == 'Invalid'
    assert check_end_2('Luck') == 'Invalid'
    assert check_end_2('Lucky123') == 'Invalid'
    assert check_end_2('ucky') == 'Invalid'

def test_check_begin():
    assert check_begin('Lucky') == 'Valid'
    assert check_begin('Ludo') == 'Valid'
    assert check_begin('lucky') == 'Invalid'
    assert check_begin('Vicky') == 'Invalid'
    assert check_begin('Lille') == 'Invalid'

def test_only_end():
    assert only_end('Lucky') == 'Valid'
    assert only_end('Happy') == 'Valid'
    assert only_end('LuckyVicky') == 'Valid'
    assert only_end('Luck') == 'Invalid'
    assert only_end('Tiki') == 'Invalid'
    assert only_end('Lucky123') == 'Invalid'

def test_check_any():
    assert check_any('a') == 1
    assert check_any('ab') == 2
    assert check_any('abc') == 3
    assert check_any('abcd') == 3
    assert check_any('abcde') == 3
    assert check_any('abc a') == 4

def test_check_any_number():
    assert check_any_number(55678) == 0
    assert check_any_number(5567) == 0
    assert check_any_number(1259) == 3
    assert check_any_number(1258) == 2

def test_check_inv_any():
    assert check_inv_any('a') == 0
    assert check_inv_any('ab') == 0
    assert check_inv_any('abc') == 0
    assert check_inv_any('abcd') == 1
    assert check_inv_any('abcde') == 2
    assert check_inv_any('abc a') == 1 # space

def test_check_non_digit():
    assert check_non_digit('a') == 1
    assert check_non_digit('ab') == 2
    assert check_non_digit('abc a') == 5 # space
    assert check_non_digit('abc1234') == 3

def test_periodic_check():
    assert periodic_check('a') == 0
    assert periodic_check('ac') == 1
    assert periodic_check('abc') == 1
    assert periodic_check('abcd') == 2
    assert periodic_check('abcde') == 2
    assert periodic_check('abc ab') == 3

def test_star():
    assert star('mn') == 1
    assert star('man') == 1
    assert star('woman') == 1
    assert star('maaan') == 1
    assert star('main') == 0
    assert star('mamaman') == 1
    assert star('manmaanmain') == 2
    assert question('an') == 0
    assert question('amn') == 1

def test_plus():
    assert plus('mn') == 0
    assert plus('man') == 1
    assert plus('woman') == 1
    assert plus('maaan') == 1
    assert plus('main') == 0
    assert plus('mamaman') == 1
    assert plus('manmaanmain') == 2
    assert question('an') == 0
    assert question('amn') == 1

def test_question():
    assert question('mn') == 1
    assert question('man') == 1
    assert question('woman') == 1
    assert question('maaan') == 0
    assert question('main') == 0
    assert question('mamaman') == 1
    assert question('manmaanmain') == 1
    assert question('an') == 0
    assert question('amn') == 1

def test_least_most():
    assert least_most('mn') == 0
    assert least_most('an') == 0
    assert least_most('amn') == 0
    assert least_most('aman') == 0
    assert least_most('aan') == 1
    assert least_most('aan naa') == 2
    assert least_most('aan naa aaanaa') == 4
    assert least_most('aaaan') == 1

def test_least_most_digit():
    assert least_most_digit('a55') == 1
    assert least_most_digit('1 2') == 0
    assert least_most_digit('1 2 3') == 0
    assert least_most_digit('14 ab667898') == 3

def test_alt():
    assert alt('mn') == 0
    assert alt('aaaa') == 4
    assert alt('ace') == 1
    assert alt('back') == 2
    assert alt('bench') == 1

def test_group():
    assert group('mn') == 0
    assert group('aaaa') == 0
    assert group('ace') == 0
    assert group('amn') == 1
    assert group('bmn') == 1
    assert group('amnbmn') == 2