from python_regex import *

def test_find_string():
    assert find_string('the sun the 56the445the the hethe') == 6

def test_find_digit():
    assert find_digit('the sun the 56the445the the hethe t548h $%ysajodfm') == ['56', '445', '548']
    assert find_digit('the s556un the 56the445the t5556687he he54the t548h $%ysajodfm') == ['556', '56', '445', '555', '668', '54', '548']

def test_find_digit_with_plus():
    assert find_digit_with_plus('the sun the 56the445the the hethe t548h $%ysajodfm') == ['56', '445', '548']
    assert find_digit_with_plus('the s556un the 56the445the t5556687he he54the t548h $%ysajodfm') == ['556', '56', '445', '5556687', '54', '548']

def test_splitting():
    assert splitting_by_space('the sun the 56the445the the hethe') == ['the', 'sun', 'the', '56the445the', 'the', 'hethe']
    assert splitting_by_digit('the s556un the 56the445the t5556687he he54the t548h $%ysajodfm') == ['the s','un the ', 'the', 'the t', 'he he', 'the t', 'h $%ysajodfm']
    assert splitting_using_max_split_arg('the s556un the 56the445the t5556687he he54the t548h $%ysajodfm', 3) == ['the s','un the ', 'the', 'the t5556687he he54the t548h $%ysajodfm']

def _2():
    assert replace_string('each day is a new day') == 'each go is a new go'
    assert replace_string('a day with a day is a day') == 'a go with a go is a go'

def test_replace_with_count():
    assert replace_with_count('each day is a new day', 1) == 'each go is a new day'
    assert replace_with_count('a day with a day is a day', 2) == 'a go with a go is a day'

def test_replace_with_count_tuple():
    assert replace_with_count_tuple('each day is a new day', 1) == ('each go is a new day', 1)
    assert replace_with_count_tuple('a day with a day is a day', 2) == ('a go with a go is a day', 2)

def test_search_string_start_index():
    assert search_string_start_index('each day is a new day') == 5
    assert search_string_start_index('a day with a day is a day') == 2

def test_search_string_end_index():
    assert search_string_end_index('each day is a new day') == 8
    assert search_string_end_index('a day with a day is a day') == 5

def test_search_string_span():
    assert search_string_span('each day is a new day') == (5, 8)
    assert search_string_span('a day with a day is a day') == (2, 5)

def test_match_string():
    assert match_string('each day do sing') == ('ch d', 'ch', 'd', ('ch', 'd'), 2, 6, (2, 6), re.compile('(\\w{2}) (d)'))

def test_get_string():
    assert get_string('each day do sing') == 'each day do sing'

def test_replace_string_with_group():
    assert replace_string_with_group('each day do sing') == 'ea<b>ch d</b>ay do sing'