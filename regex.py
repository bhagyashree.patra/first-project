#!/usr/bin/env python3

import re

def beginning(string):
    pattern = '^.u'
    if re.match(pattern, string): # can use search, findall as well
        return 'Valid'
    else:
        return 'Invalid'

def beginning_2(string):
    pattern = '\\ALu' # '\A.u' will fail
    if re.match(pattern, string):
        return 'Valid'
    else:
        return 'Invalid'

def beginning_3(string):
    pattern = '\\Athe'
    if re.match(pattern, string):
        return 'Valid'
    else:
        return 'Invalid'

def beginning_4(string):
    pattern = '^the'
    if re.match(pattern, string):
        return 'Valid'
    else:
        return 'Invalid'

def ending(string):
    pattern = 'ck.$'
    if re.search(pattern, string): # can use findall; but match will fail
        return 'Valid'
    else:
        return 'Invalid'

def ending_2(string):
    pattern = 'cky\\Z'
    if re.search(pattern, string):
        return 'Valid'
    else:
        return 'Invalid'

def beginning_or_ending(string):
    pattern = '^the|the$'
    found = re.findall(pattern, string)
    if found:
        return 'Valid'
    else:
        return 'Invalid'

def beginning_or_ending_word(string):
    pattern = '\\bthe'
    found = re.findall(pattern, string)
    if found:
        return 'Valid'
    else:
        return 'Invalid'

def not_beginning_or_ending_word(string):
    pattern = '\\Bthe\\B'
    found = re.findall(pattern, string)
    if found:
        return 'Valid'
    else:
        return 'Invalid'

def anywhere(string):
    pattern = 'ck'
    if re.search(pattern, string): # can use findall; but match will fail
        return 'Valid'
    else:
        return 'Invalid'

def anything_from_set(string):
    pattern = '[ck]'
    search = re.search(pattern, string) # can use findall 
    found = re.findall(pattern, string) # can't use search
    if search:
        return found
    else:
        return 'Invalid'

def nothing_from_set(string):
    pattern = '[^ck]'
    search = re.search(pattern, string) # can use findall 
    found = re.findall(pattern, string) # can't use search
    if search:
        return found
    else:
        return 'Invalid'

def nothing_from_set_range(string):
    pattern = '[^a-o]'
    search = re.search(pattern, string)
    found = re.findall(pattern, string)
    if search:
        return found
    else:
        return 'Invalid'

def alternation(string):
    pattern = 'u|c|k'
    search = re.search(pattern, string)
    found = re.findall(pattern, string)
    if search:
        return len(found), found
    else:
        return 'Invalid'

def group(string):
    pattern = '(a|b|c)ky'
    search = re.search(pattern, string)
    found = re.findall(pattern, string)
    if search:
        return found
    else:
        return None

def min_max(string):
    pattern = 'l{2,3}'
    search = re.search(pattern, string)
    found = re.findall(pattern, string)
    if search:
        return len(found), found
    else:
        return 'Invalid'

def min_max_range(string):
    pattern = '[0-9]{2,4}'
    search = re.search(pattern, string)
    found = re.findall(pattern, string)
    if search:
        return len(found), found
    else:
        return 'No Match'

def min_max_out_of_range(string):
    pattern = '[^0-9]{2,3}'
    search = re.search(pattern, string)
    found = re.findall(pattern, string)
    if search:
        return len(found), found
    else:
        return 'No Match'

def zero_or_more(string):
    pattern = 'ma*n'
    found = re.findall(pattern, string)
    return len(found), found

def one_or_more(string):
    pattern = 'ma+n'
    found = re.findall(pattern, string)
    return len(found)

def zero_or_one(string):
    pattern = 'ma?n'
    found = re.findall(pattern, string)
    return len(found)

def digits(string):
    pattern = '\\d'
    found = re.findall(pattern, string)
    return found

def spaces(string):
    pattern = '\\s'
    found = re.findall(pattern, string)
    return found

def alpha_numeric(string):
    pattern = '\\w'
    found = re.findall(pattern, string)
    return found

def special_chars(string):
    pattern = '\\W'
    found = re.findall(pattern, string)
    return found
